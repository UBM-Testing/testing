import fs from "fs";
import puppeteer from "puppeteer";
import util from "util";

import { browserHeight, browserWidth, headlessMode, ignoreHTTPSErrors, navigationTimeout, testTimeout, testTimeout1m, testTimeout2m, testTimeout3m, testTimeout4m, testTimeout5m } from './constants';
import { fullScreenAd, countADs } from "./tests/test.01.advertisement";
import { clickLogin, clickRegister } from "./tests/test.02.login.register";
import { footerLinksClick } from "./tests/test.03.footer";
import { menuStatus, menuContent } from "./tests/test.04.main.menu";
import { splitArticlesTest } from "./tests/test.05.splitting.articles";
import { googleAnalyticsTest } from "./tests/test.06.google.analytics";
import { shortRegistration } from "./tests/test.07.registration.testing";
import { loginTest, changeValueTest, logoutTest, checkReLogChangeValueTest } from "./tests/test.08.main.profile.manipulation.testing";
import { checkGUIDLogin } from "./tests/test.09.GUID.login.testing";
import { customAliasLogic } from "./tests/test.10.custom.aliases.logic.testing";
import { metatagCanonicalTag } from "./tests/test.11.metatag.canonicals.testing";
import { resourceTopicContent, resourceTopicHero } from "./tests/test.12.hero.check.testing";


const satellites = Array(
  'http://www.obgyn.net/',
/*  'http://www.managedhealthcareexecutive.com/',
  'http://www.ophthalmologytimes.com/',
  'http://www.dermatologytimes.com/',
  'http://www.urologytimes.com/',
  'http://www.optometrytimes.com/',
  'http://www.contemporarypediatrics.com/',
  'http://www.drugtopics.com/',
  'http://www.aestheticchannel.com/',
  'http://www.formularywatch.com/',
  'http://www.modernretina.com/',
  'http://www.contemporaryobgyn.net/',
  'http://europe.ophthalmologytimes.com/',
  'http://www.practicalcardiology.com/',
  'http://www.endocrinologynetwork.com/',
  'http://www.medicaleconomics.com/',
  'http://www.cancernetwork.com/',
  'http://www.patientcareonline.com/',
  'http://www.rheumatologynetwork.com/',
  'http://www.psychiatrictimes.com/',
  'http://www.neurologytimes.com/',
  'http://www.diagnosticimaging.com/',
  'http://www.theaidsreader.com/',
  'http://www.nutritionaloutlook.com/',
  'http://www.pediatricsconsultantlive.com/',
  'http://www.oncotherapynetwork.com/',
  'http://www.physicianspractice.com/',*/
  ////'http://www.cannabissciencetech.com'
);

let page;
let browser;

const log_file = fs.createWriteStream(__dirname + '/test-report.html', { flags: 'w' });
const log_stdout = process.stdout;

log_file.write('<head><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"></head><body>');

console.log = function (d) { //
  log_file.write('<p class="text-primary">' + util.format(d) + '</p>');
  log_stdout.write(util.format(d) + '\n');
};
console.warn = function (d) { //
  log_file.write('<p class="text-warning">' + util.format(d) + '</p>');
  log_stdout.write(util.format(d) + '\n');
};
console.error = function (d) { //
  log_file.write('<p class="text-danger">' + util.format(d) + '</p>');
  log_stdout.write(util.format(d) + '\n');
};

// before all and after all module
beforeAll(async () => {
  browser = await puppeteer.launch({
    headless: headlessMode,
    ignoreHTTPSErrors: ignoreHTTPSErrors,
    //slowMo: 30,
    args: [
      '--no-sandbox ',
      '--disable-setuid-sandbox',
      `--window-size=${browserWidth},${browserHeight}`
      //'--start-fullscreen'
    ]
  });
  page = await browser.newPage();
  await page.setViewport({ height: browserHeight, width: browserWidth });
  await page.setDefaultNavigationTimeout(navigationTimeout);
});

afterEach(async () => {
  let pages = await browser.pages();
  if (pages.length > 2) {
    await pages[2].close();
    pages = '';
  }
  pages = '';
});

afterAll(() => {
  browser.close();
});

satellites.forEach(function (site) {
  let testResult;

  describe("Testing site: " + site, () => {

    test("ID 1.1 Welcome ad appearance check", async () => {
      testResult = await fullScreenAd(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout);

    /*
    test("ID 1.2 Count number of visible ADs", async () => {
      testResult = await countADs(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout);

    test("ID 2.1 Check clicking LOGIN button in the header of main page", async () => {
      testResult = await clickLogin(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 2.2 Check clicking REGISTER button in the header of main page", async () => {
      testResult = await clickRegister(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 3.1 Check clicking on all footer links of site, redirection is succeeded", async () => {
      testResult = await footerLinksClick(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 4.1 Main menu testing, status", async () => {
      testResult = await menuStatus(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout3m);

    test("ID 4.2 Main menu status, content", async () => {
      testResult = await menuContent(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout3m);

    test("ID 5.1 Check articles dividing", async () => {
      testResult = await splitArticlesTest(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout1m);

    test("ID 6.1 Test that Google Analytics is present on the site", async () => {
      testResult = await googleAnalyticsTest(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout);

    test("ID 7.1 Registration (short version)", async () => {
      testResult = await shortRegistration(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout1m);

    test("ID 8.1 Login test ", async () => {
      testResult = await loginTest(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 8.2 Change value test 1", async () => {
      testResult = await changeValueTest(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 8.3 Logout test", async () => {
      testResult = await logoutTest(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 8.4 Change value test 2", async () => {
      testResult = await checkReLogChangeValueTest(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout2m);

    test("ID 9.1 Check GUID login ", async () => {
      testResult = await checkGUIDLogin(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout1m);

    test("ID 10.1 Check custom alias logic test, all articles into Resource Topics (first page) are checked", async () => {
      testResult = await customAliasLogic(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout1m);

    test("ID 11.1 Metatag / canonicals test, every article or each resource topics has Canonical teg and this teg contains at least topic URL", async () => {
      testResult = await metatagCanonicalTag(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout5m);

    test("ID 12.1 Check each Resource Topic, has a hero", async () => {
      testResult = await resourceTopicHero(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';

    }, testTimeout1m);

    test("ID 12.2 Check each Resource Topic, has a content", async () => {
      testResult = await resourceTopicContent(site, page, console);
      expect(testResult).toBeTruthy();
      testResult = '';
    }, testTimeout1m);
*/
  });
});