import { navigationTimeout, testTimeout, testTimeout1m, testTimeout2m, testTimeout3m, testTimeout4m, testTimeout5m } from './constants';


//login
const LOGIN_USERNAME_INPUT_SELECTOR = '#edit-name';
const LOGIN_PASSWORD_INPUT_SELECTOR = '#edit-pass';
const LOG_IN_BUTTON_SELECTOR = '#edit-submit';
//
const MAIL = 'satellite.d7@gmail.com'
const PASSWORD = 'Satellited7';
//logout
const LOGIN_SELECTOR = 'a[href="/user/login"]';
const LOGOUT_SELECTOR = 'a[href="/user/logout"]';

let response;


exports.closeWelcomeAd = async (page) => {
  let welcomeAd = await page.$$('#close-button > img');
  if (welcomeAd.length > 0) {
    await page.waitForSelector('#close-button > img');
    await page.click('#close-button > img');
    await page.waitFor(1500);
  }
  welcomeAd = '';
}

exports.login = async (site, page) => {
  await page.goto(site + '/user/login', { waitUntil: "load" });
  await page.waitForSelector(LOGIN_USERNAME_INPUT_SELECTOR, { timeout: 58000, visible: true });
  await exports.closeWelcomeAd(page);
  page.evaluate(_ => {
    window.scrollBy(0, window.innerHeight);
  });
  [response] = await Promise.all([
    page.waitForNavigation(),
    await page.click(LOGIN_USERNAME_INPUT_SELECTOR),
    await page.type(LOGIN_USERNAME_INPUT_SELECTOR, MAIL),
    await page.click(LOGIN_PASSWORD_INPUT_SELECTOR),
    await page.type(LOGIN_PASSWORD_INPUT_SELECTOR, PASSWORD),
    await page.hover(LOG_IN_BUTTON_SELECTOR),
    await page.click(LOG_IN_BUTTON_SELECTOR)
  ]);
  expect(response._status).toBe(200);
  response = '';
  await page.waitForSelector(LOGOUT_SELECTOR);
}

exports.logout = async (page) => {
  await page.waitForSelector(LOGOUT_SELECTOR);
  await exports.closeWelcomeAd(page);
  [response] = await Promise.all([
    page.waitForNavigation(),
    await page.waitForSelector(LOGOUT_SELECTOR),
    await page.click(LOGOUT_SELECTOR)]);
  expect(response._status).toBe(200);
  response = '';
  await exports.closeWelcomeAd(page);
}, testTimeout1m

exports.logoutShortWithUrl = async (site, page) => {
  await page.goto(site + '/user/logout', { waitUntil: "load" });
  await page.waitForSelector(LOGIN_SELECTOR);
  await exports.closeWelcomeAd(page);
}, testTimeout1m