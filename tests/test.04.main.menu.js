const closeWelcomeAd = require('../helpers').closeWelcomeAd;
// variables
let count;
let response;
let content;
let unusualContent;

const MAIN_MENU_SELECTOR = '.brandingNavigation .leaf';

//tests



exports.menuStatus = async (site, page, console) => {
  var i;
  try {
    await page.goto(site, { waitUntil: "domcontentloaded" });

    //await page.goto(site, { waitUntil: ["networkidle2", "load"] });
    //count number of menu tiles
    await page.waitForSelector(MAIN_MENU_SELECTOR);
    let newTabCheck = await page.$$('.brandingNavigation .leaf a[target="_blank"]');
    if (newTabCheck.length > 0) {
      console.error(site + '  menu contains '+ newTabCheck.length +' links which are opened in a new tab, please fix it');
    }
    count = await page.$$(MAIN_MENU_SELECTOR);
    for (i = 1; i < count.length + 1; i++) {
      // click on appropriate tile
      await closeWelcomeAd(page);
      [response] = await Promise.all([
        page.waitForNavigation(),
        page.waitForSelector('.brandingNavigation li:nth-child(' + i + ')'),
        page.click('.brandingNavigation li:nth-child(' + i + ')')
      ]);
      expect(response._status).toBe(200);
      //go back to main page if there was a redirection to new site
      if (!response._url.includes(site)) {
        await Promise.all([
          page.waitForNavigation(),
          await page.goBack()]);
      }
      response = '';
      //console.log(site + ' main menu tile number ' + i + ' clicked successfully');
    }
    count = '';
    return true;
  }
  catch (error) {
    console.error('ID 4.1 Main menu testing, status ' + site + ' looks like there was an issue during clicking on .brandingNavigation li:nth-child(' + i + ') '  + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-4.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}


exports.menuContent = async (site, page, console) => {
  var i;
  try {
    await page.goto(site, { waitUntil: "domcontentloaded" });

    //await page.goto(site, { waitUntil: ["networkidle2", "load"] });
    //count number of menu tiles
    await page.waitForSelector(MAIN_MENU_SELECTOR);
    count = await page.$$(MAIN_MENU_SELECTOR);
    for (i = 1; i < count.length + 1; i++) {
      // click on appropriate tile
      await closeWelcomeAd(page);
      [response] = await Promise.all([
        page.waitForNavigation(),
        page.waitForSelector('.brandingNavigation li:nth-child(' + i + ')'),
        page.click('.brandingNavigation li:nth-child(' + i + ')')
      ]);
      // check appearance of link in content block
      content = await page.$$('.mainContent a[href]');
      //page www.diagnosticimaging.com/editorial%20board has unusual content thats why we need add one more variable 'unusualContent' as a trigger
      unusualContent = await page.$$('.flex-container li');
      //calculation of whole content
      content = content.length + unusualContent.length;
      if (content == 0 && response._url.includes(site)) {
        console.warn('ID 4 - Main menu test: ' + response._url + ' has no content')
        //expect(content).toBeGreaterThan(0)
      }
      content = '';
      unusualContent = '';
      //go back to main page if there was a redirection to new site
      if (!response._url.includes(site)) {
        await Promise.all([
          page.waitForNavigation(),
          await page.goBack()]);
      }
      response = '';
    }
    count = '';
    return true;
  } catch (error) {
    console.error('ID 4.2 Main menu status, content ' + site + ' looks like there was an issue during clicking on .brandingNavigation li:nth-child(' + i + ') '  + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-4.2.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}
