
//footer selectors
const FOOTER_LINKS_COLUMNS_SELECTOR = '.expanded .menu';

//tests
exports.footerLinksClick = async (site, page, console) => {
  let footerLinks;
  let r;
  
  try {
    await page.goto(site, { waitUntil: "load" });
    await page.waitForSelector(FOOTER_LINKS_COLUMNS_SELECTOR);
    //checking footer for a links which opened in a new tabs
    let newTabCheck = await page.$$('.expanded a[target="_blank"]');
    if (newTabCheck.length > 0) {
      console.warn(site + '  footer contains links which are opened in a new tab, please fix it');
    }
    newTabCheck = '';
    // add all footer links into array
    footerLinks = await page.evaluate(
      () => Array.from(document.body.querySelectorAll('.expanded a[href]'), ({ href }) => href)
    );
    for (r = 0; r < footerLinks.length; r++) {
      // for now we have a problem with navigation to pdf pages in headless mode so we a skip testing such pages, for now
       if (footerLinks[r] == 'http://marketing.advanstar.info/mediakits/TC_MK_2016.pdf') {
      console.warn('ID 3.1 Check clicking on all footer links of site: we cannot check ' + footerLinks[r] + ' of ' + site + ' , we are working on a solving this problem');
      } else {
      // navigate to each link from array
      let [response] = await Promise.all([
        page.waitForNavigation(),
        await page.goto(footerLinks[r], { waitUntil: "load" }),
      ]);
      // check status of respond
      if (response._status == 206) {
        expect(response._status).toBe(206);
      } else {
        expect(response._status).toBe(200);
      }
      response = '';
    }
  }
    return true;
  }
  catch (error) {
    console.error('ID 3.1 Check clicking on all footer links of site, ' + site + ' : Looks like there was an error during clicking on ' + footerLinks[r]  + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-3.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}

