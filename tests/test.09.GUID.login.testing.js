const closeWelcomeAd = require('../helpers').closeWelcomeAd;
const GUID = '?GUID=25AA8E3E-7B05-4CF7-81ED-ABF06E21DA58';
//login
const LOG_IN_BUTTON_SELECTOR = '#edit-submit';
// variables
let count;


//tests
exports.checkGUIDLogin = async (site, page, console) => {
  try {
    await page.goto(site + GUID, { waitUntil: "load" });
    await page.waitForSelector('#custom-user-links');
    await closeWelcomeAd(page);
    await Promise.all([
      page.waitForNavigation(),
      await page.$$eval("a", as => as.find(a => a.innerText.includes("Edit Profile")).click()),
      await page.waitForSelector(LOG_IN_BUTTON_SELECTOR),
      count = await page.$$(LOG_IN_BUTTON_SELECTOR)
    ]);
    expect(count.length).toBe(1);
    count = '';
    return true;
  }
  catch (error) {
    console.error("Error during GUID automation on site " + site + GUID + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-9.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}
