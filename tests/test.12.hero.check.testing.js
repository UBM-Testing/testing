const closeWelcomeAd = require('../helpers').closeWelcomeAd;
let content;
//tests
let resourceTopicsHeroVar;
let resourceTopicContentVar;

exports.resourceTopicHero = async (site, page, console) => {
  try {
    //navigate to a site
    await page.goto(site, { waitUntil: "domcontentloaded" });
    await closeWelcomeAd(page);
    //place all Resource Topics hrefs into Array 
    resourceTopicsHeroVar = await page.evaluate(
      () => Array.from(document.body.querySelectorAll('.block-ubm-common .links li a[href]'), ({ href }) => href)
    );
    for (let i = 0; i < resourceTopicsHeroVar.length; i++) {
      // navigate to each Resource Tipic
      await page.goto(resourceTopicsHeroVar[i], { waitUntil: "domcontentloaded" });
      await page.waitForSelector('.view-content');
      await closeWelcomeAd(page);
      let hero = await page.$$('.featured-article');
      if (hero.length < 1) {
        console.warn('ID 12 - Hero check test : ' + resourceTopicsHeroVar[i] + ' - has no hero')
      }
      hero = '';
    }
    return true;
  }
  catch (error) {
    console.error("Error during topic hero testing " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-12.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}

exports.resourceTopicContent = async (site, page, console) => {
  try {
    await page.goto(site, { waitUntil: "load" });
    //place all Resource Topics hrefs into Array     
    resourceTopicContentVar = await page.evaluate(
      () => Array.from(document.body.querySelectorAll('.block-ubm-common .links li a[href]'), ({ href }) => href)
    );
    // navigate to each Resource Topic
    for (let i = 0; i < resourceTopicContentVar.length; i++) {
      await page.goto(resourceTopicContentVar[i], { waitUntil: "load" });
      // count content  
      content = await page.$$('.views-row');
      if (content.length == 0) {
        console.warn('ID 12 - Hero check test : ' + resourceTopicContentVar[i] + ' has no content')
        expect(content).toBeGreaterThan(0)
      }
      content = '';
    }
    return true;
  }
  catch (error) {
    console.error("Error during topic content testing " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-12.2.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}