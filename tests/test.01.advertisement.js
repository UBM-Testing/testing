const closeWelcomeAd = require('../helpers').closeWelcomeAd;

//logo selectors
const BRAND_LOGO_SELECTORS = '.brandLogo';
//footer selectors
const FOOTER_LINKS_COLUMNS_SELECTOR = '.expanded .menu';
//content
const RECENT_CONTENT_SELECTOR = '.view-header';
//social media
const SOCIAL_MEDIA_MENU_SELECTOR = '.socialMediaChannels';

//tests

exports.fullScreenAd = async (site, page, console) => {
  try {
    await page.goto(site, { waitUntil: "load" });
    let fsAd = await page.$$('#creative > a > img');
    if (fsAd.length == 1) {
      await closeWelcomeAd(page);
      console.log("ID 1.1 Welcome ad appearance check: site " + site + " has Welcome ad");
      fsAd = '';
      return true;
    } else {
      fsAd = '';
      console.log("ID 1.1 Welcome ad appearance check: site " + site + " has no Welcome ad");
      return true;
    }
  }
  catch (error) {
    console.error("Error during Welcome ad testing on " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-1.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}

exports.countADs = async (site, page, console) => {
  try {
    await page.goto(site, { waitUntil: "load" });
    // selectors for hover on the site for loading lazy ADs
    var allAds = Array(
      BRAND_LOGO_SELECTORS,
      RECENT_CONTENT_SELECTOR,
      SOCIAL_MEDIA_MENU_SELECTOR,
      FOOTER_LINKS_COLUMNS_SELECTOR
    );
    //hover on each place of the site for loading lazy ADs
    await page.waitForSelector(allAds[0]);
    await closeWelcomeAd(page);
    for (var i = 0; i < allAds.length; i++) {
      await page.waitForSelector(allAds[i]);
      await page.hover(allAds[i]);
      await page.waitFor(1000);
    }
    //count number of ADs with ad-visible ad-loaded labels
    let loadedAndVisibleArticles = await page.$$('.dfp-tag-wrapper.ad-visible.ad-loaded');
    if (loadedAndVisibleArticles.length > 0) {
      console.warn('ID 1.2 Count number of visible ADs: ' + site + ' site has ' + loadedAndVisibleArticles.length + ' loaded and visible blocks of ADs')
      loadedAndVisibleArticles = '';
      return true;
    } else {
      console.error('ID 1.2 Count number of visible ADs: ' + site + ' site has ' + loadedAndVisibleArticles.length + ' loaded and visible blocks of ADs')
      loadedAndVisibleArticles = '';
      return false;
    }
  }
  catch(error) {
    console.error("Error during ADs count testing on " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-1.2.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}
