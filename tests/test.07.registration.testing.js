import faker from "faker";

const closeWelcomeAd = require('../helpers').closeWelcomeAd;

const NAME = 'PuppeteerTestName'; //All new users will have this name during the registration

//register
const REGISTER_E_MAIL_INPUT_SELECTOR = '#edit-mail';
const REGISTER_PASSWORD_INPUT_SELECTOR = '#edit-pass-pass1';
const REGISTER_CONFIRM_PASSWORD_INPUT_SELECTOR = '#edit-pass-pass2';
const REGISTER_AR_U_HEALTH_PROF_DROPDOWN_SELECTOR = '#edit-field-health-care-pro-und';
const REGISTER_FIRST_NAME_INPUT_SELECTOR = '#edit-field-first-name-reg-und-0-value';
const REGISTER_LAST_NAME_INPUT_SELECTOR = '#edit-field-last-name-reg-und-0-value';
const REGISTER_PROFESSION_DROPDOWN_SELECTOR = '#edit-field-profession-no-hc-und';
const REGISTER_COUNTRY_DROPDOWN_SELECTOR = '#edit-field-work-country-und';
const REGISTER_ZIP_CODE_INPUT_SELECTOR = '#edit-field-work-zip-code-und-0-value';
const REGISTER_CREATE_NEW_BUTTON_SELECTOR = '#edit-submit';
const ERROR_MESSAGE_SELECTOR = '.messagesContent';
// variables

let generatedPassword;
let selectDropElem;
let response;


//tests

exports.shortRegistration = async (site, page, console) => {
  try {
    await page.goto(site + '/user/register', { waitUntil: "load" });
    await page.waitForSelector(REGISTER_E_MAIL_INPUT_SELECTOR);
    await closeWelcomeAd(page);
    generatedPassword = await faker.internet.password();
    generatedPassword = generatedPassword + '1';
    [response] = await Promise.all([
      page.waitForNavigation(),
      page.evaluate(_ => { window.scrollBy(0, window.innerHeight); }),
      await page.click(REGISTER_E_MAIL_INPUT_SELECTOR),
      await page.type(REGISTER_E_MAIL_INPUT_SELECTOR, faker.internet.email()),
      await page.click(REGISTER_PASSWORD_INPUT_SELECTOR),
      await page.type(REGISTER_PASSWORD_INPUT_SELECTOR, generatedPassword),
      page.evaluate(_ => { window.scrollBy(0, window.innerHeight); }),
      await page.click(REGISTER_CONFIRM_PASSWORD_INPUT_SELECTOR),
      await page.type(REGISTER_CONFIRM_PASSWORD_INPUT_SELECTOR, generatedPassword),
      page.evaluate(_ => { window.scrollBy(0, window.innerHeight); }),
      selectDropElem = await page.$(REGISTER_AR_U_HEALTH_PROF_DROPDOWN_SELECTOR),
      await selectDropElem.type('No'),
      await page.click(REGISTER_FIRST_NAME_INPUT_SELECTOR),
      await page.type(REGISTER_FIRST_NAME_INPUT_SELECTOR, NAME),
      await page.click(REGISTER_LAST_NAME_INPUT_SELECTOR),
      await page.type(REGISTER_LAST_NAME_INPUT_SELECTOR, faker.name.lastName()),
      page.evaluate(_ => { window.scrollBy(0, window.innerHeight); }),
      selectDropElem = await page.$(REGISTER_PROFESSION_DROPDOWN_SELECTOR),
      await selectDropElem.type('Press/Media'),
      selectDropElem = await page.$(REGISTER_COUNTRY_DROPDOWN_SELECTOR),
      await selectDropElem.type('United States'),
      await page.click(REGISTER_ZIP_CODE_INPUT_SELECTOR),
      await page.type(REGISTER_ZIP_CODE_INPUT_SELECTOR, faker.address.zipCode()),
      await page.click(REGISTER_CREATE_NEW_BUTTON_SELECTOR),
    ]);
    expect(response._status).toBe(200);
    response = '';
    page.waitForSelector(ERROR_MESSAGE_SELECTOR);
    await closeWelcomeAd(page);
    return true;
  }
  catch (error) {
    console.error("There was an error during short registration on " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-7.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}