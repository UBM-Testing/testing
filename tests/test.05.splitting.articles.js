const closeWelcomeAd = require('../helpers').closeWelcomeAd;
//articles
const SECOND_PAGE_SELECTOR = '[title="Go to page 2"]';
// variables
let response;

//tests

exports.splitArticlesTest = async (site, page, console) => {
   /* Local func definition */
    async function checkTab(articleUrl) 
    {
      try 
      {
        [response] = await Promise.all([
          page.waitForNavigation(),
          page.goto(site + articleUrl, { waitUntil: "load" })
        ]);
        expect(response._status).toBe(200);
        response = '';
        await page.waitForSelector(SECOND_PAGE_SELECTOR);
        await closeWelcomeAd(page);
        [response] = await Promise.all([
          page.waitForNavigation(),
          await page.waitForSelector(SECOND_PAGE_SELECTOR),
          await page.click(SECOND_PAGE_SELECTOR)
        ]);
        expect(response._status).toBe(200);
        expect(response._url).toBe(site + articleUrl + '/page/0/1');
        response = '';
      }
      catch (error) 
      {
        console.error("Error during testing " + site + articleUrl + ' </br> ' + error);
        site = site.replace('http://', '');
        site = site.replace('/', '');
        await page.screenshot({path: 'screenshots/' + site + '-5.1.jpeg', type: 'jpeg', fullPage: true});
        return false;
      }
    }

    switch (true) 
    {
      case site.includes('cancernetwork'):
        await checkTab('aacr-street-team/uncovering-epigenetic-targets-cancer');
        break;
      case site.includes('rheumatologynetwork'):
        await checkTab('rheumatoid-arthritis/obesity-not-linked-refractory-rheumatoid-arthritis');
        break;
      case site.includes('dermatologytimes'):
        await checkTab('derm-nps-and-pas/pearls-optimize-safety-success-biologics');
        break;
      case site.includes('endocrinologynetwork'):
        await checkTab('blog/primary-hyperparathyroidism-pearls');
        break;
      case site.includes('www.obgyn'):
        await checkTab('infertility/office-hysteroscopy-infertility');
        break;
      case site.includes('diagnosticimaging'):
        await checkTab('case-studies/sudden-cardiac-arrest-during-ct-scan');
        break;
      case site.includes('managedhealthcareexecutive'):
        await checkTab('mhe-articles/end-life-care-planning-advancements-what-health-execs-should-know');
        break;
      case site.includes('www.ophthalmologytimes'):
        await checkTab('modern-medicine-cases/lens-fragmentation-device-loop');
        break;
      case site.includes('www.urologytimes'):
        await checkTab('modern-medicine-feature-articles/active-surveillance-linked-moderate-anxiety');
        break;
      case site.includes('optometrytimes'):
        await checkTab('optometrytimes/news/how-vaping-affects-ocular-surface');
        break;
      case site.includes('contemporarypediatrics'):
        await checkTab('contemporary-pediatrics/news/best-tech-pediatrics-2016');
        break;
      case site.includes('aestheticchannel'):
        await checkTab('cosmetic-surgery/search-office-space');
        break;
      case site.includes('drugtopics'):
        await checkTab('editors-choice-drtp/your-new-role-battle-against-depression');
        break;
      case site.includes('formularywatch'):
        await checkTab('feature-articles/drugs-perspective-myalept-metreleptin');
        break;
      case site.includes('modernretina'):
        await checkTab('article/safe-dosing-hcq-dependent-body-type');
        break;
      case site.includes('contemporaryobgyn'):
        await checkTab('contemporary-obgyn/news/maternal-death-intracranial-hemorrhage-0');
        break;
      case site.includes('practicalcardiology'):
        await checkTab('practical-cardiology/news/hypertension-management-complicated-hyperkalemia-whats-your-rx');
        break;
      case site.includes('europe.ophthalmologytimes'):
        await checkTab('ophthalmologytimes/news/private-practice-no-more-one-man-job');
        break;
      case site.includes('patientcareonline'):
        await checkTab('pain/man-multiple-short-lasting-unilateral-daily-headaches');
        break;
      case site.includes('psychiatrictimes'):
        await checkTab('obsessive-compulsive-disorder/strategies-treatment-resistant-ocd');
        break;
      case site.includes('neurologytimes'):
        await checkTab('blog/finding-consensus-treating-myasthenia-gravis');
        break;
      case site.includes('theaidsreader'):
        await checkTab('hiv-aids/very-early-art-does-curative-window-exist');
        break;
      case site.includes('nutritionaloutlook'):
        await checkTab('trends-business/it-time-hemp-cbd-dietary-supplements-industry-panic-not-all-say-insiders');
        break;
      case site.includes('pediatricsconsultantlive'):
        await checkTab('pediatric-skin-diseases/streptococcus-intertrigo-0');
        break;
      case site.includes('oncotherapynetwork'):
        console.log('For now this site has no 2 page articles, as soon we have at least one we will make the test for it');
        break;
      case site.includes('cannabissciencetech'):
        await checkTab('article/tips-and-technology-advance-your-grow-facility');
        break;
      case site.includes('medicaleconomics'):
        await checkTab('technology/diy-telemedicine');
        break;
      case site.includes('physicianspractice'):
        await checkTab('article/face-opioid-crisis');
        break;
      default:
        console.log('Looks like you trying to test site witch is out of our range');
    }
    
    return true; 
}