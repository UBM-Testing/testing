
// variables
const closeWelcomeAd = require('../helpers').closeWelcomeAd;
let response;



//tests

  exports.clickLogin = async (site, page, console) => {
    try {
      await page.goto(site, { waitUntil: "domcontentloaded" });
      await page.waitForSelector('a[href="/user/login"]');
      await closeWelcomeAd(page);
      [response] = await Promise.all([
        page.waitForNavigation(),
        await page.$$eval("a", as => as.find(a => a.innerText.includes("Login")).click()),
        await page.waitForSelector('.password-link')
      ]);
      expect(response._status).toBe(200);
      response = '';
      return true;
    }
    catch(error) {
      console.error('ID 2.1 Check clicking LOGIN button in the header of main page of ' + site + ' error during test' + '</br>' + error);
      response = '';
      site = site.replace('http://', '');
      site = site.replace('/', '');
      await page.screenshot({path: 'screenshots/' + site + '-2.1.jpeg', type: 'jpeg', fullPage: true});
      return false;
    }
  }

  exports.clickRegister = async (site, page, console) => {
    try {
      await page.goto(site, { waitUntil: "domcontentloaded" });
      await page.waitForSelector('a[href="/user/register"]');
      await closeWelcomeAd(page);    
      [response] = await Promise.all([
        page.waitForNavigation(),
        await page.$$eval("a", as => as.find(a => a.innerText.includes("Register")).click()),
        await page.waitForSelector('#edit-pass-pass2')
      ]);
      expect(response._status).toBe(200);
      response = '';
      return true;
    }
    catch(error) {
      console.error('ID 2.2 Check clicking REGISTER button in the header of main page of ' + site + ' error during test' + ' </br> ' + error);
      response = '';
      site = site.replace('http://', '');
      site = site.replace('/', '');
      await page.screenshot({path: 'screenshots/' + site + '-2.2.jpeg', type: 'jpeg', fullPage: true});
      return false;
    }
  }

