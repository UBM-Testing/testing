const closeWelcomeAd = require('../helpers').closeWelcomeAd;

const login = require('../helpers').login;
const logout = require('../helpers').logout;
const logoutShortWithUrl = require('../helpers').logoutShortWithUrl;
const newName = 'Newname';
const oldName = 'Oldname';
//register
const REGISTER_FIRST_NAME_INPUT_SELECTOR = '#edit-field-first-name-reg-und-0-value';
//edit page
const EDIT_SAVE_SELECTOR = '#edit-submit';
// variables
let saveMessage;
let response;

const clientNumberInDataBase = {
  'http://www.psychiatrictimes.com/': '',
  'http://www.patientcareonline.com/': '',
  'http://www.cancernetwork.com/': 'user/206147/edit',
  'http://www.diagnosticimaging.com/': '',
  'http://www.physicianspractice.com/': '',
  'http://www.obgyn.net/': 'UA--43',
  'http://www.pediatricsconsultantlive.com/': '',
  'http://www.rheumatologynetwork.com/': '',
  'http://www.theaidsreader.com/': '',
  'http://www.oncotherapynetwork.com/': '',
  'http://www.endocrinologynetwork.com/': '',
  'http://www.neurologytimes.com/': '',
  'http://www.contemporaryobgyn.net/': '',
  'http://www.contemporarypediatrics.com/': '',
  'http://www.aestheticchannel.com/': '',
  'http://www.dermatologytimes.com/': '',
  'http://www.drugtopics.com/': '',
  'http://www.formularywatch.com/': '',
  'http://www.managedhealthcareexecutive.com/': '',
  'http://www.medicaleconomics.com/': 'user/20774/edit',
  'http://www.modernretina.com/': '',
  'http://www.ophthalmologytimes.com/': '',
  'http://www.optometrytimes.com/': '',
  'http://www.practicalcardiology.com/': '',
  'http://www.urologytimes.com/': '',
  'http://europe.ophthalmologytimes.com/': '',
  'http://www.nutritionaloutlook.com/': '',
  'http://www.cannabissciencetech.com/': ''
};

//tests
exports.loginTest = async (site, page, console) => {
  try {
    await login(site, page);
    return true;
  }
  catch (error) {
    console.error("Error during LOGIN to the " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({ path: 'screenshots/' + site + '-8.1.jpeg', type: 'jpeg', fullPage: true });
    return false;
  }
}

exports.changeValueTest = async (site, page, console) => {
  try {
    // editing of name, saving the changes
    // await Promise.all([
    //   page.waitForNavigation(),
    //   await page.$$eval("a", as => as.find(a => a.innerText.includes("Edit Profile")).click())
    // ]);
    await page.goto(site + '/user', { waitUntil: "load" });


    await page.waitForSelector(REGISTER_FIRST_NAME_INPUT_SELECTOR);
    await closeWelcomeAd(page);
    [response] = await Promise.all([
      page.waitForNavigation(),
      await page.$eval(REGISTER_FIRST_NAME_INPUT_SELECTOR, REGISTER_FIRST_NAME_INPUT_SELECTOR => REGISTER_FIRST_NAME_INPUT_SELECTOR.value = ''),
      await page.type(REGISTER_FIRST_NAME_INPUT_SELECTOR, newName),
      await page.click(EDIT_SAVE_SELECTOR)
    ]);
    expect(response._status).toBe(200);
    response = '';
    // check correct alert appear
    await page.waitForSelector('.messagesContent', { timeout: 59000, visible: true });
    await closeWelcomeAd(page);
    await page.waitForSelector('.messagesContent', { timeout: 59000, visible: true });
    saveMessage = await page.evaluate(() => document.querySelector('.messagesContent').textContent);
    expect(saveMessage).toContain('The changes have been saved.');
    saveMessage = '';
    return true;
  }
  catch (error) {
    console.error("Error during change name at " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({ path: 'screenshots/' + site + '-8.2.jpeg', type: 'jpeg', fullPage: true });
    return false;
  }
}

exports.logoutTest = async (site, page, console) => {
  try {
    await logout(page);
    return true;
  }
  catch (error) {
    console.error("Error during LOGOUT at the " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({ path: 'screenshots/' + site + '-8.3.jpeg', type: 'jpeg', fullPage: true });
    return false;
  }
}

exports.checkReLogChangeValueTest = async (site, page, console) => {
  try {
    // login for checking that after re-login changes still saved
    await login(site, page);
    // copy modified field, and changing it to old value
    let username;
    await Promise.all([
      page.waitForNavigation(),
      await page.$$eval("a", as => as.find(a => a.innerText.includes("Edit Profile")).click())
    ]);
    await page.waitForSelector(REGISTER_FIRST_NAME_INPUT_SELECTOR);
    await closeWelcomeAd(page);
    [response] = await Promise.all([
      page.waitForNavigation(),
      username = await page.evaluate(() => { return document.querySelector('#edit-field-first-name-reg-und-0-value').value; }),
      await page.$eval(REGISTER_FIRST_NAME_INPUT_SELECTOR, REGISTER_FIRST_NAME_INPUT_SELECTOR => REGISTER_FIRST_NAME_INPUT_SELECTOR.value = ''),
      await page.type(REGISTER_FIRST_NAME_INPUT_SELECTOR, oldName),
      await page.click(EDIT_SAVE_SELECTOR)
    ]);
    expect(response._status).toBe(200);
    response = '';
    //checking that modified field is correct
    expect(username).toBe(newName);
    // logout

    //await logout(page);


    await logoutShortWithUrl(site, page);

    return true;
  }
  catch (error) {
    console.error("Error during checking changes made in 'ID 8.2 Change value test 1' after LOGOUT/LOGIN at the " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({ path: 'screenshots/' + site + '-8.4.jpeg', type: 'jpeg', fullPage: true });
    return false;
  }
}