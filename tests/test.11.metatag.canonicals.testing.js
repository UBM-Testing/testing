const closeWelcomeAd = require('../helpers').closeWelcomeAd;

//tests
exports.metatagCanonicalTag = async (site, page, console) => {
  try {
    await page.goto(site, { waitUntil: "domcontentloaded" });
    await closeWelcomeAd(page);
    //place all Resource Topics hrefs into Array 
    let resourceTopics = await page.evaluate(
      () => Array.from(document.body.querySelectorAll('.block-ubm-common .links li a[href]'), ({ href }) => href)
    );
    for (let i = 0; i < resourceTopics.length; i++) {
      // navigate to each Resource Topic
      await page.goto(resourceTopics[i], { waitUntil: "domcontentloaded" });
      await closeWelcomeAd(page);
      // put all Resource Topic articles into array
      let topicsArticles = await page.evaluate(
        () => Array.from(document.body.querySelectorAll('.view-content .node-title a[href]'), ({ href }) => href)
      );
      //check that all articles has appropriate Resource Topic into URL
      for (let b = 0; b < topicsArticles.length; b++) {
        let canonical = await page.goto(topicsArticles[b], { waitUntil: "domcontentloaded" });
        //console.log(topicsArticles[b]);
        expect(await canonical.text()).toContain('<link rel=\"canonical\" href=\"' + 'http://');
        // expect(await canonical.text()).toContain('<link rel=\"canonical\" href=\"' + site);

        var regex = /<link rel="canonical" href="([^"]+)"/gmi;
        var resultCan = regex.exec(await canonical.text());
        if (resultCan[1].length <= site.length + 1) {
          console.warn('ID 11 - Metatag / canonicals test: ' + topicsArticles[b] + ' - topic has wrong canonical');
        }
        resultCan = '';
        canonical = '';
      }
    }
    return true;
  }
  catch (error) {
    console.error("Error during metatag canonical test " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-11.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}