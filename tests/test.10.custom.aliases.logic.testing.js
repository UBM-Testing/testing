const closeWelcomeAd = require('../helpers').closeWelcomeAd;

//tests
exports.customAliasLogic = async (site, page, console) => {
  try {
    await page.goto(site, { waitUntil: "domcontentloaded" });
    await closeWelcomeAd(page);
    //place all Resource Topics hrefs into Array 
    let resourceTopics = await page.evaluate(
      () => Array.from(document.body.querySelectorAll('.block-ubm-common .links li a[href]'), ({ href }) => href)
    );
    for (let i = 0; i < resourceTopics.length; i++) {
      // navigate to each Resource Topic
      await page.goto(resourceTopics[i], { waitUntil: "domcontentloaded" });
      await closeWelcomeAd(page);
      // put all Resource Topic articles into array
      let topicsArticles = await page.evaluate(
        () => Array.from(document.body.querySelectorAll('.view-content .node-title a[href]'), ({ href }) => href)
      );
      //check that all articles has appropriate Resource Topic into URL
      for (let b = 0; b < topicsArticles.length; b++) {
        if (!topicsArticles[b].includes(resourceTopics[i])) {
          console.warn('ID 10 - Custom aliases logic: custom alias working wrong, topic ' + resourceTopics[i] + ' contains wrong article ' + topicsArticles[b]);
        }
      }
    }
    return true;
  }
  catch(error) {
    console.error("Error during custom alias logic testing " + site + ' </br> ' + error);
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-10.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
  }