const googleAnalyticsCode = {
  'http://www.psychiatrictimes.com/': 'UA-630614-1',
  'http://www.patientcareonline.com/': 'UA-630614-10',
  'http://www.cancernetwork.com/': 'UA-630614-16',
  'http://www.diagnosticimaging.com/': 'UA-630614-18',
  'http://www.physicianspractice.com/': 'UA-630614-23',
  'http://www.obgyn.net/': 'UA-630614-43',
  'http://www.pediatricsconsultantlive.com/': 'UA-630614-44',
  'http://www.rheumatologynetwork.com/': 'UA-630614-45',
  'http://www.theaidsreader.com/': 'UA-630614-48',
  'http://www.oncotherapynetwork.com/': 'UA-630614-49',
  'http://www.endocrinologynetwork.com/': 'UA-630614-51',
  'http://www.neurologytimes.com/': 'UA-630614-52',
  'http://www.contemporaryobgyn.net/': 'UA-630614-56',
  'http://www.contemporarypediatrics.com/': 'UA-630614-57',
  'http://www.aestheticchannel.com/': 'UA-630614-58',
  'http://www.dermatologytimes.com/': 'UA-630614-59',
  'http://www.drugtopics.com/': 'UA-630614-60',
  'http://www.formularywatch.com/': 'UA-630614-61',
  'http://www.managedhealthcareexecutive.com/': 'UA-630614-62',
  'http://www.medicaleconomics.com/': 'UA-630614-63',
  'http://www.modernretina.com/': 'UA-630614-64',
  'http://www.ophthalmologytimes.com/': 'UA-630614-65',
  'http://www.optometrytimes.com/': 'UA-630614-67',
  'http://www.practicalcardiology.com/': 'UA-630614-68',
  'http://www.urologytimes.com/': 'UA-630614-69',
  'http://europe.ophthalmologytimes.com/': 'UA-630614-71',
  'http://www.nutritionaloutlook.com/': 'UA-35480831-1',
  'http://www.cannabissciencetech.com/': 'UA-56138753-7'
};

exports.googleAnalyticsTest = async (site, page, console) => {
  const homepage = await page.goto(site, { waitUntil: "load" });
  const homepageSourceCode = await homepage.text();
  if (homepageSourceCode.includes(googleAnalyticsCode[site])) {
    //console.log('ID 6.1 Test that Google Analytics is present on the site: ' + site + ' - Google Analytics code is present on the page.');
    return true;
  }
  else {
    console.warn('ID 6.1 Test that Google Analytics is present on the site: ' + site + ' - Google Analytics code is NOT present on the page.');
    site = site.replace('http://', '');
    site = site.replace('/', '');
    await page.screenshot({path: 'screenshots/' + site + '-6.1.jpeg', type: 'jpeg', fullPage: true});
    return false;
  }
}