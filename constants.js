// The third argument (optional) is timeout (in milliseconds) for specifying how long to wait before aborting
// See https://jestjs.io/docs/en/api#testname-fn-timeout
module.exports.testTimeout = 32000;
module.exports.testTimeout1m = 61000;
module.exports.testTimeout2m = 120000;
module.exports.testTimeout3m = 180000;
module.exports.testTimeout4m = 240000;
module.exports.testTimeout5m = 300000;

//navigation timeout (after this timeout if page or element will not be loaded test will fail)
module.exports.navigationTimeout = 60000;

//screen resolution
module.exports.browserHeight = 800;
module.exports.browserWidth = 1280;

// headless mode true/false testing
module.exports.headlessMode = true;

// do we need to ignore HTTPS errors?
module.exports.ignoreHTTPSErrors = true;